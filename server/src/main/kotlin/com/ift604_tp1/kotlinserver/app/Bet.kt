package com.ift604_tp1.kotlinserver.app

import com.google.gson.JsonObject

class Bet(user: String, amount: Int, equipe : Int, match : Int) {
    public var user : String = user;
    public var montant : Int = amount;
    public var equipeId : Int = equipe
    public var matchId : Int = match
    public var status : Boolean = false //false for ongoing match, true for ended match
    public var valeurFinale : Double? = null

    public fun toJson() : JsonObject {
        var obj = JsonObject()
        obj.addProperty("user", user)
        obj.addProperty("amount", montant)
        obj.addProperty("matchId", matchId)
        obj.addProperty( "status" ,status)
        obj.addProperty("valeurFinale", valeurFinale)
        return obj
    }
}
