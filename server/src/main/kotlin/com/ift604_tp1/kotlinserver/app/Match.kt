package com.ift604_tp1.kotlinserver.app

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import java.text.SimpleDateFormat
import java.util.*

class Match (_id: Int, _equipe1: String, _equipe2 : String, _date : Date) {

    public var equipe1 : Equipe = Equipe(_equipe1);
    public var equipe2 : Equipe = Equipe(_equipe2);
    public var dateDebut : Date = _date;
    public var id : Int = _id;

    fun getName(): String {
        return equipe1.nomEquipe + " VS. " + equipe2.nomEquipe;
    }

    fun format(d : Date) : String{
        val sdf = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
        return sdf.format(d)
    }
    fun toJson(): JsonObject {
        var obj = JsonObject()
        obj.addProperty("id",id)
        obj.addProperty("date", format(dateDebut))
        obj.add("equipe1", equipe1.toJson())
        obj.add("equipe2", equipe2.toJson())
        return obj;
    }

    fun remainingTime() : Long {
        var remainingTime = 60 - (Date().time - dateDebut.time)/60000
        if(remainingTime > 0 ) return remainingTime
        return 0
    }

    fun hasEnded(): Boolean {
        var matchEnd: Calendar = Calendar.getInstance()
        matchEnd.time = dateDebut
        matchEnd.add(Calendar.HOUR, 1)
        return ! (Date().before(matchEnd.time) && Date().after(dateDebut))
    }
}

