package com.ift604_tp1.kotlinserver.app

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.HashMap

public class ServiceMatch(listeMatch:  HashMap<Int, Match> ){
    private var listeDeMatch : HashMap<Int, Match> = listeMatch;
    private var id : Int = 1;
    
    public fun getAllMatches() : HashMap<Int, Match> {
        return listeDeMatch;
    }

    public fun getMatch(id: Int) : Match? {
        return listeDeMatch[id];
    }


    fun addSampleData(){
        var date: Calendar = Calendar.getInstance()
        date.time = Date()
        date.add(Calendar.HOUR, -2)

        var date2: Calendar = Calendar.getInstance()
        date2.time = Date()
        date2.add(Calendar.MINUTE, -42)

        var m1: Match = Match(getNextId(), "Blues", "Reds", Date())
        var m2: Match = Match(getNextId(), "Oranges", "Greens", date2.time);
        var m3: Match = Match(getNextId(), "Yellows", "Browns", Date());
        var m4: Match = Match(getNextId(), "Cyans", "Bluebirds", Date());
        var m5: Match = Match(getNextId(), "TemperateYellow", "CyanOrange", date.time);
        m5.equipe1.score()
        m5.equipe1.score()

        AddToMap(m1);
        AddToMap(m2);
        AddToMap(m3);
        AddToMap(m4);
        AddToMap(m5);
    }

    fun getNextId  (): Int {
        return id++;
    }
    fun AddToMap( match : Match){
        listeDeMatch[match.id] = match;
    }

    fun getRandomTeam() : Equipe{
        var i : Int = listeDeMatch.count()
        var randomNumber = (1..i).shuffled().first()
        var m : Match = listeDeMatch[randomNumber]!!;
        while(m.hasEnded()){
            var rand = (1..i).shuffled().first()
            m = listeDeMatch[rand]!!
        }
        print("Match " + m.id + " ")
        if(randomBool()) {
            print("team 1: "+ m.equipe1.nomEquipe + " ")
            return m.equipe1;
        } else {
            print("team 2 "+ m.equipe2.nomEquipe+ " ")
            return m.equipe2
        }
    }
    //TEST FUNCTION
    fun launchMockDataGenerator(){
        var i : Int = 0;
        GlobalScope.launch {
            while(true){
                var e: Equipe = getRandomTeam()
                if(randomBool()){
                    e.score()
                    println(" SCORE")
                } else {
                    var p = Penalite(Date(), (1..50).shuffled().first())
                    e.penalites.add(p)
                    println(" PENALITE")
                }
                delay(20000)
            }
        }
    }
}
