package com.ift604_tp1.kotlinserver.app

import com.beust.klaxon.Klaxon
import com.google.gson.JsonArray
import com.google.gson.JsonDeserializer
import com.google.gson.JsonObject
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.routing.*
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

import java.util.*
import kotlin.collections.HashMap

import io.ktor.features.CORS
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod


public var listeDeMatch :  HashMap<Int, Match> =  hashMapOf<Int, Match>()
public var listeBet :  HashMap<Int, ArrayList<Bet>> = hashMapOf<Int, ArrayList<Bet>>()

fun randomBool() : Boolean {
    return (0..1).shuffled().first() == 0
}


fun main(args: Array<String>){
    var sm = ServiceMatch(listeDeMatch)
    sm.addSampleData();
    sm.launchMockDataGenerator()

    var sb = ServiceBet(listeDeMatch, listeBet)
    sb.addSampleData()

    embeddedServer(Netty,23567) {
        install(CORS){
            method(HttpMethod.Options)
            header(HttpHeaders.XForwardedProto)
            anyHost()
            host("localhost")
            allowCredentials = true
        }
        routing {
            get("matches") {
                var listeDeMatch = sm.getAllMatches()
                var obj = JsonObject()
                listeDeMatch.forEach {
                    obj.add(it.key.toString(), it.value.toJson())
                }
                call.respond(HttpStatusCode.OK, obj.toString())

            }
            get("/matches/current"){
                var listeDeMatch = sm.getAllMatches()
                var anHourAgo : Calendar = Calendar.getInstance();
                anHourAgo.add(Calendar.HOUR, -1);
                var obj : JsonArray = JsonArray()
                listeDeMatch.forEach {
                    var matchEnd: Calendar = Calendar.getInstance()
                    matchEnd.time = it.value.dateDebut
                    matchEnd.add(Calendar.HOUR, 1)
                    if(!it.value.dateDebut.before(anHourAgo.time) && Date().before(matchEnd.time )){
                        obj.add(it.key)
                    }
                }
                call.respond(HttpStatusCode.OK, obj.toString())
            }
            get("/matches/{id}") {
                var match : Match? = sm.getMatch(call.parameters["id"]!!.toInt())
                var obj = match!!.toJson().toString()
                call.respond(HttpStatusCode.Accepted, obj)
            }
            post("/matches/{idMatch}/equipes/{idEquipe}/bet/"){
                try{
                    val idMatch = call.parameters["idMatch"]!!.toInt();
                    var post = call.receive<String>();
                    var res : BetPostData = Klaxon().parse<BetPostData>(post.toString())!!

                    var bool = sb.bet( idMatch, call.parameters["idEquipe"]!!.toInt(), res.user!!,res.amount!!)
                    if(bool){
                        call.respond(HttpStatusCode.OK)
                    } else {
                        call.respond(HttpStatusCode.InternalServerError)
                    }
                } catch (e : Exception){
                    println(e.toString());
                }


            }
            get("/bets/{user}") {
                var user : String = call.parameters["user"]!!
                var obj = sb.getBetsForUser(user)
                call.respond(HttpStatusCode.Accepted, obj.toString())
            }
        }
    }.start(wait = true)
}
