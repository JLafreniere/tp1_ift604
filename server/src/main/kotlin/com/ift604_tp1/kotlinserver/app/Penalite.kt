package com.ift604_tp1.kotlinserver.app

import com.google.gson.JsonObject
import java.text.SimpleDateFormat
import java.util.*

class Penalite(_time: Date, _playerNo: Int) {
    val sdf = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
    public var time : Date = _time;
    public var playerNo: Int = _playerNo;

    public fun toJson() : JsonObject{
        var pen = JsonObject()
        pen.addProperty("joueur", playerNo)
        pen.addProperty("temps", sdf.format(time))
        return pen
    }
}