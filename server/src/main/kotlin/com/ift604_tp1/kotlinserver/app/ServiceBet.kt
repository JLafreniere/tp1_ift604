package com.ift604_tp1.kotlinserver.app

import com.google.gson.JsonArray
import com.google.gson.JsonObject

class ServiceBet(listeMatch : HashMap<Int, Match>, listeBet : HashMap<Int, ArrayList<Bet>>) {
    public final var bets = listeBet
    public final var listeMatch = listeMatch;

    public fun addSampleData(){
        bet(5, 1, "tom", 100, true)
        bet(5, 2, "jean", 300, true)
        bet(1, 1, "tom", 100, true)
        bet(2, 2, "marc", 200, true)
    }

    public fun bet(idMatch : Int, idEquipe : Int, userName : String, amount: Int, ignoreRestriction: Boolean = false) : Boolean{

        if(bets[idMatch]==null){
            bets[idMatch] = arrayListOf<Bet>()
        }
        val remainingTime = listeMatch[idMatch]!!.remainingTime()

        if(remainingTime >= 20 || ignoreRestriction) {
            //Check if there is an existing bet

            var exists : Boolean = false
            bets[idMatch]!!.forEach{
                if(it.user == userName) {
                    it.equipeId = idEquipe;
                    it.montant = amount //Update bet
                    exists = true
                }
            }
            if(!exists){
                bets[idMatch]!!.add(Bet(userName, amount, idEquipe, idMatch)) //Create Bet
            }

            return true
        } else {
            return false
        }
    }

    public fun getBetsForUser(user: String): JsonArray {

        var betsForUser = hashMapOf<Int, Bet>()

        bets.forEach{ match ->
            match.value.forEach{
                if(it.user == user){
                    betsForUser[match.key] = Bet(it.user, it.montant, it.equipeId, it.matchId)
                    if(listeMatch[it.matchId]!!.hasEnded()){

                        betsForUser[match.key]!!.status = true

                        var amountTeam1 : Double = 0.0
                        var amountTeam2 : Double = 0.0
                        var myAmount = it.montant
                        match.value.forEach { betEntry ->
                            if(betEntry.equipeId == 1){
                                amountTeam1 += betEntry.montant.toDouble()
                            } else {
                                amountTeam2 += betEntry.montant.toDouble()
                            }
                        }

                        var gainPercentage : Double = 0.0
                        if(it.equipeId == 1){
                            gainPercentage = myAmount/amountTeam1
                            betsForUser[match.key]!!.valeurFinale = amountTeam2 * 0.75 * gainPercentage
                        } else {
                            println("CALCUL GAIN POUR PARIEUR SUR EQUIPE 2")
                            gainPercentage = myAmount/amountTeam2
                            betsForUser[match.key]!!.valeurFinale = amountTeam1 * 0.75 * gainPercentage
                        }
                    }
                }
            }
        }

        var arr = JsonArray()

        betsForUser.forEach{ entry ->
            var value = entry.value
            var obj = JsonObject()
            obj.addProperty("matchId", value.matchId)
            obj.addProperty("equipeId", value.equipeId)
            obj.addProperty("user", value.user)
            obj.addProperty("montant", value.montant)
            obj.addProperty("status", value.status)
            obj.addProperty("valeurFinale", value.valeurFinale)
            arr.add(obj)
        }

        return arr
    }
}