package com.ift604_tp1.kotlinserver.app

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Equipe(_nomEquipe: String) {
    public var nomEquipe : String = _nomEquipe;
    public var penalites : ArrayList<Penalite> = arrayListOf<Penalite>();
    public var buts : ArrayList<But> = arrayListOf<But>(); //Integer representing the second of the match the goal was scored
    val sdf = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
    public fun score(){

        val player = (1..50).shuffled().first()
        buts.add(But(player, Date()))
    }

    public fun toJson() : JsonObject {
        var obj =  JsonObject()
        obj.addProperty("nom", nomEquipe)

        var penalitesArr = JsonArray()

        penalites.forEach{
            penalitesArr.add(it.toJson())
        }

        obj.add("penalites", penalitesArr)

        var butsArr = JsonArray()

        buts.forEach{
            var obj : JsonObject= JsonObject()
            obj.addProperty("player", it.player)
            obj.addProperty("date", sdf.format(it.date))
            butsArr.add(obj)
        }

        obj.add("buts", butsArr)

        var betsArr = JsonArray()

        return obj;

    }
}